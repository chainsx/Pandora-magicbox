# Pandora-magicbox
### 注意，这个项目的性质正如这个名字一般，只要打开了，就没什么好东西

* 首先，感谢cgoxopx以及dx_chaos（dixin_ren）的思路，

* 要直播什么的，选择YRSSF吧！

* 这个可以说是破解专版，现在最常用的还是在[这里](https://github.com/Erblocker/Pandora-magicbox/blob/master/end-method/README.md)的适用于安卓的方法

* 当然，还支持路由，某某派（RaspberryPi,nanopi,orangepi）

* 现在，你可以浏览[NoPad](https://nopad.org)主页来获取更多信息

### 对于安卓

在/arch/ARM/Linux/Android里的方法是用ksweb搭建的，比较复杂，如果你需要ksweb的话，选择此方法

在/end-method里的是用修改过的AndroPHP来搭建，由于修改过，不适合用做其他用途，但架设方法简单得多
